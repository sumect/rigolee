#!/bin/sh

#######################################################
#
#Print page shell
#Paramaters
# p1 0-print test page, 1-print screen
# p2 count ot page
# p3 size  of page
# p4 ip    of printer
# p5 port  of printer
# p6 driver of printer
#
#Return
# 0-Printing
# 1-Network disconnected
# 2-Printer is busy
#######################################################



PRINT_COPY=$2
PRINT_SIZE=$3

########################################################
#Network printer IP
########################################################

PRINT_SERV=$4
PRINT_PORT=$5
PRINT_DRIV=$6

########################################################
# File will be printed
########################################################
if [ $1 -ne 0 ]; then
PRINT_FILE=/tmp/snap.bmp
else
PRINT_FILE=/rigol/cups/testPage.bmp
fi

########################################################
#Printing status
########################################################
PRINT_STAT=/tmp/printer.txt


########################################################
#cancel all jobs queued
########################################################
exit_print()
{
	lpadmin -x dsprinter
	rm /rigol/cups/var/log/cups/*log >/dev/null 2>&1
	exit $1
}


########################################################
# ping network printer
########################################################
ping $PRINT_SERV  -w 2 -c 2 >/dev/null 2>&1
if [ $? -ne 0 ]; then
	echo '1'
	exit 1
fi

########################################################
# Add printer of dsprinter
########################################################

lpadmin -p dsprinter -E -v socket://$PRINT_SERV:$PRINT_PORT -P /rigol/cups/share/cups/model/$PRINT_DRIV
sleep 0.5
lpstat -p > $PRINT_STAT
########################################################
# check printer is IDLE
########################################################
cat $PRINT_STAT | grep 'is idle' >/dev/null 2>&1
if [ $? -ne 0 ]; then
	echo '2'
	exit_print 2
fi

########################################################
# check printer is NOT busy
########################################################
cat $PRINT_STAT | grep 'is busy' >/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo '2'
	exit_print 2
fi

########################################################
# Print the screen to /tmp/snap.bmp saved in memory
########################################################
#if [ $1 -ne 0 ]; then
#/rigol/socketDemo ":display:data?" >/dev/null 2>&1
#fi

#echo 'OK'
#sleep 0.5
########################################################
# Connect to printer and print
########################################################
#
#
# -d dsprinter   printer name
# -o landscape   print for horizontal
# -o fit-to-page  specifies that the document should be scaled to fit on the page
# -n 1 copies of print
# -o media=a4    PageSize=A4
#
/rigol/cups/bin/lp -d dsprinter -o landscape -o fit-to-page -n $PRINT_COPY -o media=$PRINT_SIZE $PRINT_FILE >/dev/null 2>&1
sleep 0.5
########################################################
# Wait for finishing printing
########################################################
for i in $(seq 30);
do
	lpstat -p > $PRINT_STAT

	cat $PRINT_STAT| grep 'is busy' >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo '2'
		exit_print 2
	fi

	cat $PRINT_STAT| grep 'now printing' >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo '0'
		sleep 0.5
	else
		exit_print 0
	fi

done



